import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { ButtonGroup, Button } from "react-bootstrap";
import Mynav from "./Component/Mynav";
import MyTable from "./Component/MyTable";
import AddInfo from "./Component/AddInfo";
import Cardinfo from "./Component/Cardinfo";
import * as onAction from "./Function/onAction";
import { v4 as uuidv4 } from "uuid";
import ModalInfo from "./Component/ModalInfo";

export default function App() {
  const [persons, setPersons] = useState(onAction.arrpersons);
  const [view, setView] = useState(onAction.arrpersons[0]);
  var [bool, setBool] = useState(false);
  let [num, setNum] = useState(0);

  const [show, setShow] = useState(false);

  const handleClose = () => {
    setShow(false);
  };
  const handleShow = (index) => {
    for (let i = 0; i < persons.length; i++) {
      if (persons[i].id == index) {
        setNum(i);
      }
    }

    setShow(true);
  };

  //onDeleteHandler
  const onDeleteHandler = (index) => {
    console.log(index);
    const temp = [...persons];
    for (let i = 0; i < temp.length; i++) {
      if (temp[i].id === index) {
        temp.splice(i, 1);
      }
    }
    setPersons(temp);
  };

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [gender, setGender] = useState("male");
  const [position, setPosition] = useState();

  const getValueName = (event) => {
    event.preventDefault();
    setName(event.target.value);
  };
  const getValueEmail = (event) => {
    event.preventDefault();
    setEmail(event.target.value);
  };
  const getValueGender = (event) => {
    event.preventDefault();
    setGender(event.target.value);
  };
  const getValuePosition = (event) => {
    event.preventDefault();
    setPosition(event.target.value);
  };

  //addInformation
  const onAddHandler = (event) => {
    event.preventDefault();
    document.getElementById("username").value = "";
    document.getElementById("email").value = "";
    const newList = persons.concat({
      id: uuidv4(),
      name,
      gender,
      email,
      position,
    });
    setPersons(newList);
  };

  return (
    <>
      <Mynav />
      <br />

      <div style={{ margin: "0 auto", width: "70%" }}>
        <h1>Person Info</h1>
        <br />
        <AddInfo
          getValueName={getValueName}
          getValueEmail={getValueEmail}
          getValueGender={getValueGender}
          getValuePosition={getValuePosition}
          onAddHandler={onAddHandler}
          onbool={bool}
        />
        <br />
        <h3>Display Data as:</h3>
        <br />
        <div>
          <ButtonGroup aria-label="Basic example">
            <Button variant="secondary" onClick={onAction.changeToTable}>
              Table
            </Button>
            <Button variant="dark" onClick={onAction.changeToCard}>
              Card
            </Button>
          </ButtonGroup>
        </div>
        <br />
        <div id="table">
          <MyTable
            persons={persons}
            onDelete={onDeleteHandler}
            myopen={handleShow}
            myclose={handleClose}
            show={show}
            num={num}
          />
        </div>

        <div id="card" style={{ display: "none" }}>
          <div
            style={{
              display: "flex",
              flexFlow: "row wrap",
            }}
          >
            <Cardinfo
              people={persons}
              myopen={handleShow}
              myclose={handleClose}
              show={show}
              num={num}
            />
          </div>
        </div>
      </div>
    </>
  );
}
