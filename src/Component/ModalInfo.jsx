import React, { useState } from "react";
import { Modal, NavItem } from "react-bootstrap";

export default function ModalInfo({ show, theclose, myperson, mynum }) {


  return (
    <div>
      <Modal show={show} onHide={theclose}>
        <Modal.Header>
          <Modal.Title>{myperson[mynum].name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            <b>Gender:</b> {myperson[mynum].gender}
          </p>
          <p>
            <b>Email:</b> {myperson[mynum].email}
          </p>
          <p>
            <b>Position: </b>
            {myperson[mynum].position}
          </p>
        </Modal.Body>
      </Modal>
    </div>
  );
}
