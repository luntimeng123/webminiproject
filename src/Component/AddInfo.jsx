import React from "react";
import { Form, Button, Col, Row } from "react-bootstrap";

export default function AddInfo({
  getValueName,
  getValueEmail,
  getValueGender,
  getValuePosition,
  onAddHandler,
  onbool,
}) {
  return (
    <div>
      <Form>
        <div className="row">
          <div className="col-lg-8">
            {/* group input */}
            <Form.Group>
              <Form.Label>Username</Form.Label>
              <Form.Control
                type="text"
                placeholder="Username"
                name="username"
                id="username"
                onChange={getValueName}
              />
              {/* <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text> */}
            </Form.Group>
            <br />
            <Form.Group>
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="email"
                placeholder="Email"
                name="email"
                id="email"
                onChange={getValueEmail}
              />

              {/* <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text> */}
            </Form.Group>
            <br />
            {/* btn submit */}
            <Button
              disabled={onbool}
              variant="primary"
              type="submit"
              onClick={onAddHandler}
            >
              Submit
            </Button>{" "}
            <Button variant="secondary" type="submit">
              Cancel
            </Button>
          </div>

          {/* group checkbox */}
          <div className="col-lg-4">
            <h4>Gender</h4>
            <div>
              <Form.Check
                type="radio"
                label="Male"
                value="male"
                name="gender"
                id="r1"
                checked={true}
                onChange={getValueGender}
              />
              <Form.Check
                type="radio"
                label="Female"
                value="female"
                name="gender"
                id="r2"
                onChange={getValueGender}
              />
            </div>
            <br />
            {/* position */}
            <div style={{ display: "flex", justifyContent: "space-around" }}>
              <Form.Check
                aria-label="option 1"
                label={`Student`}
                value="student"
                onChange={getValuePosition}
              />
              <Form.Check
                aria-label="option 1"
                label={`Teacher`}
                value="teacher"
                onChange={getValuePosition}
              />
              <Form.Check
                aria-label="option 1"
                label={`Developer`}
                value="developer"
                onChange={getValuePosition}
              />
            </div>
          </div>
        </div>
      </Form>
    </div>
  );
}
