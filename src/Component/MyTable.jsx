import React, { useState } from "react";
import { table, Button, ButtonGroup, Modal } from "react-bootstrap";
import * as onAction from "../Function/onAction";
import ModalInfo from "./ModalInfo";

export default function MyTable({
  persons,
  onDelete,
  myopen,
  myclose,
  show,
  num
}) {

  return (
    <div>
      <table className="table">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Gender</th>
            <th>Email</th>
            <th>Position</th>
            <th>Create At</th>
            <th>Upadte At</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {persons.map((item) => (
            <tr key={item.id}>
              <td>{item.id}</td>
              <td>{item.name}</td>
              <td>{item.gender}</td>
              <td>{item.email}</td>
              <td>{item.position}</td>
              <td>Create</td>
              <td>Upadte</td>
              <td>
                <ButtonGroup aria-label="Basic">
                  <Button variant="primary" onClick={() => myopen(item.id)}>
                    View
                  </Button>
                  <Button variant="warning">Update</Button>
                  <Button variant="danger" onClick={() => onDelete(item.id)}>
                    Delete
                  </Button>
                  <div>
                    <ModalInfo show={show} theclose={myclose} myperson={persons} mynum = {num}/>
                  </div>
                </ButtonGroup>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
