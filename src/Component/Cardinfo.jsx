import React,{useState} from "react";
import {Card,Button} from 'react-bootstrap';
import * as onAction from '../Function/onAction'
import ModalInfo from "./ModalInfo";

export default function Cardinfo({people,myopen, myclose,show,num}) {

  // const [persons,setPersons] = useState(people);
 
  return (
    <>
      {people.map(item=>(
        <Card style={{ width: '18rem',margin:"5px 5px" }} key={item.id}>
        <Card.Body>
          <Card.Title>{item.name}</Card.Title>
          <Card.Text>{item.gender}</Card.Text>
          <Card.Text>{item.email}</Card.Text>
          <Button variant="primary" onClick={() => myopen(item.id)}>view</Button>
          <ModalInfo show={show} theclose={myclose} myperson={people} mynum = {num}/>
        </Card.Body>
      </Card>
      ))}
    </>
  );
}
